### docker-git-alpine

A useful simple git container running in alpine Linux, especially for tiny Linux distro, such as RancherOS, which doesn't have a package manager.

[![DockerHub Badge](http://dockeri.co/image/99freelas/git)](https://hub.docker.com/r/99freelas/git/)

### Usage

    docker run -ti --rm -v ${HOME}:/root -v $(pwd):/git 99freelas/git <git_command>

For example, if you need clone this repository, you can run

    docker run -ti --rm -v ${HOME}:/root -v $(pwd):/git 99freelas/git clone https://bitbucket.org/docker-99freelas/docker-git.git
    
### Optional usage 1:

To save your type, create a `git` file in `~/bin/` with the following code:

```bash
#!/bin/sh

set -e

echo "git running inside docker at ~/bin/git"
docker run -it --rm -v ${HOME}:/root -v $(pwd):/git 99freelas/git "$@"
```

(Make sure that `~/bin` is in your `PATH`)

for example, if you need clone this repository, with the function you just set, you can run it as local command

    git clone https://bitbucket.org/docker-99freelas/docker-git.git

### Optional usage 2:

    alias git="docker run -ti --rm -v $(pwd):/git -v $HOME/.ssh:/root/.ssh 99freelas/git"
    
#### NOTES:

- You need redefine (re-run) the alias, when you switch between different repositories
- You need run above alias command only under git repository's root directory.
    
### The Protocols

Supports git, http/https and ssh protocols.

Refer:
[Git on the Server - The Protocols](https://git-scm.com/book/en/v2/Git-on-the-Server-The-Protocols)
